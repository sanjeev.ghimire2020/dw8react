import React, { useState } from 'react'

const LearnUseStatePracice2 = () => {
  let [count,setCount] = useState(0)
  return (
    <div>
      Count is {count}
      <br></br>
      <br></br>
      <button onClick={()=>{setCount(count+1)}}>Increment</button>
      <br></br>
      <button onClick={()=>{setCount(count+1)}}>Decrement</button>
      <br></br>
      <button onClick={()=>{setCount(0)}}>Reset</button>
    </div>
  )
}

export default LearnUseStatePracice2