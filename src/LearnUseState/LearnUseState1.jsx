import React, {useState} from 'react'

const LearnUseState1 = () => {
  let [show,setShow] = useState(true)
  return (
    <div>
      {show ? <img src='./favicon.ico' alt = 'image'></img> : null}
   
    <br></br>
    <button 
      onClick={()=>{
        setShow(true)
      }}
    > SHOW</button>
    <br></br>
    <button
      onClick={()=>{
        setShow(false)
      }} 
    > HIDE </button>
    </div>
  )
}

export default LearnUseState1