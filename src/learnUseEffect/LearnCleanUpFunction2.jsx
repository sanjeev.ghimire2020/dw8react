import React, { useEffect } from 'react'

const LearnCleanUpFunction2 = () => {

    useEffect(()=>{
        console.log("I am Use Effect")
        return()=>{
            console.log("I am Clean Up function")
        }
    }, [])
  return (
    <div>LearnCleanUpFunction2</div>
  )
}

export default LearnCleanUpFunction2